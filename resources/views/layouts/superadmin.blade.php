<!DOCTYPE html>
<html lang="en">
<head>
    <title>SUPERADMIN | PPDB MBS KOTA BEKASI 2017</title>
    <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{ url('assets/images/favicon.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ url('assets/images/favicon.ico') }}" type="image/x-icon">

    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <!-- iconfont -->
    <link rel="stylesheet" type="text/css" href="{{ url('assets/icon/icofont/css/icofont.css') }}">

    <!-- simple line icon -->
    <link rel="stylesheet" type="text/css" href="{{ url('assets/icon/simple-line-icons/css/simple-line-icons.css') }}">

    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.min.css') }}">

    <!-- Chartlist chart css -->
    <link rel="stylesheet" href="{{ url('assets/plugins/charts/chartlist/css/chartlist.css') }}" type="text/css" media="all">

    <!-- Weather css -->
    <link href="{{ url('assets/css/svg-weather.css') }}"" rel="stylesheet">

    <!-- Echart js -->
    <script src="{{ url('assets/plugins/charts/echarts/js/echarts-all.js') }}"></script>

    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/main.css') }}">

    <!-- Responsive.css-->
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/responsive.css') }}">    <!--color css-->

</head><body class="horizontal-fixed fixed">
<div class="wrapper">
    <div class="loader-bg">
        <div class="loader-bar">
        </div>
    </div>
    <!-- Navbar-->
    <header class="main-header-top hidden-print">
        <a href="index.php" class="logo"></a>
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button--><a href="../" data-toggle="offcanvas" class="sidebar-toggle hidden-md-up"></a>
            <!-- Navbar Right Menu-->
            <div class="navbar-custom-menu">
                <ul class="top-nav">
                    <!--Notification Menu-->


                    <!-- window screen -->
                    <li class="pc-rheader-submenu">
                        <a href="#!" class="drop icon-circle" onclick="javascript:toggleFullScreen()">
                            <i class="icon-size-fullscreen"></i>
                        </a>

                    </li>
                    <!-- User Menu-->
                    <li class="dropdown">
                        <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle drop-image">
                            <span><img class="img-circle " src="{{ url('assets/images/avatar-1.png') }}" style="width:40px;" alt="User Image"></span>
                            <span>{{ session('email')  }} <i class=" icofont icofont-simple-down"></i></span>

                        </a>
                        <ul class="dropdown-menu settings-menu">


                            <li class="p-0">
                                <div class="dropdown-divider m-0"></div>
                            </li>


                            <li><a href="{{ url('/superadmin/logout') }}"><i class="icon-logout"></i> Logout</a></li>

                        </ul>
                    </li>
                </ul>

            </div>
        </nav>
    </header>
    <!-- Side-Nav-->
    <aside class="main-sidebar hidden-print ">
        <section class="sidebar" id="sidebar-scroll">
            <div class="user-panel">
            </div>
            <!--horizontal Menu Starts-->
            <ul class="sidebar-menu">
                <li class="nav-level">Navigation</li>

                <li class="treeview"><a href="../admin"><i class="icon-screen-desktop"></i>ADMIN Control Panel</a></li>

            </ul>
            <!--horizontal Menu Ends-->
        </section>
    </aside>


    <div class="content-wrapper">


        @yield('content')
    </div>

    <!-- Required Jqurey -->
    <script src="{{ url('assets/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ url('assets/js/jquery-ui.min.js') }}'"></script>
    <script src="{{ url('assets/js/tether.min.js') }}"></script>

    <!-- Required Fremwork -->
    <script src="{{ url('assets/js/bootstrap.min.js') }}" ></script>

    <!-- waves effects.js -->
    <script src="{{ url('assets/plugins/waves/js/waves.min.js') }}"></script>

    <!-- Scrollbar JS-->
    <script src="{{ url('assets/plugins/slimscroll/js/jquery.slimscroll.js') }}'"></script>
    <script src="{{ url('assets/plugins/slimscroll/js/jquery.nicescroll.min.js') }}"></script>

    <!--classic JS-->
    <script src="{{ url('assets/plugins/search/js/classie.js') }}'"></script>

    <!-- notification -->
    <script src="{{ url('assets/plugins/notification/js/bootstrap-growl.min.js') }}"></script>

    <!-- Sparkline charts -->
    <script src="{{ url('assets/plugins/charts/sparkline/js/jquery.sparkline.js') }}'"></script>

    <!-- Counter js  -->
    <script src="{{ url('assets/plugins/countdown/js/waypoints.min.js') }}"></script>
    <script src="{{ url('assets/plugins/countdown/js/jquery.counterup.js') }}'"></script>

    <!-- custom js -->
    <script type="text/javascript" src="{{ url('assets/js/main.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/pages/dashboard.js') }}'"></script>
    <script type="text/javascript" src="{{ url('assets/pages/elements.js') }}"></script>
    <script src="{{ url('assets/js/menu-horizontal.js') }}'"></script>


</body>
</html>