<a href="{{ url('superadmin/sekolah') }}">TAMBAH SEKOLAH</a>
<a href="{{ url('superadmin/account') }}">TAMBAH ADMIN</a>
<a href="{{ url('superadmin/gantipassword') }}">GANTI PASSWORD</a>


@if(Session::has('nextdownload'))
    <a target="_blank" class="btn btn-info waves-effect waves-light m-r-30" href="{{  url("simpan/".Session::get('nextdownload')) }}"> Download PDF LOGIN File</a>
@endif
<table>
    <tr>
        <th>NAMA SEKOLAH</th>
        <th>ALAMAT</th>
        <th>RT</th>
        <th>RW</th>
        <th>KELURAHAN</th>
        <th>KECAMATAN</th>

        <th>KOTA</th>
    </tr>
    @foreach($sekolah as $data)
        <tr>
            <td>{{ $data->nama  }}</td>
            <td>{{ $data->alamat  }}</td>
            <td>{{ $data->rt  }}</td>
            <td>{{ $data->rw  }}</td>
            <td>{{ $data->kelurahan  }}</td>
            <td>{{ $data->kecamatan  }}</td>
        </tr>
    @endforeach
</table>

<table>
    <tr>
        <th>NAMA SISWA</th>
        <th>ALAMAT</th>
        <th>RT</th>
        <th>RW</th>
        <th>KELURAHAN</th>
        <th>KECAMATAN</th>


        <th>SEKOLAH</th>
        <td><a href="{{ url('sekolah/reset/'.$data->id)  }}">RESET PASSWORD</a></td>
    </tr>
    @foreach($siswa as $data)
        <tr>
            <td>{{ $data->nama  }}</td>
            <td>{{ $data->alamat  }}</td>
            <td>{{ $data->rt  }}</td>
            <td>{{ $data->rw  }}</td>
            <td>{{ $data->kelurahan  }}</td>
            <td>{{ $data->kecamatan  }}</td>
            <td>{{ $data->sekolah->nama  }}</td>
            <td><a href="{{ url('siswa/reset/'.$data->id)  }}">RESET PASSWORD</a></td>

        </tr>
    @endforeach
</table>
