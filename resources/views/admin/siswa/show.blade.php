@extends('layouts.admin')
@section('content')

 <div class="container-fluid">
    <!-- Row Starts -->
    <div class="row">
      <div class="col-sm-12 p-0">
        <div class="main-header">
          <h4>Daftar Calon Siswa</h4>
          <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
            <li class="breadcrumb-item">
              <a href="{{ url('admin/siswa') }}">
                <i class="icofont icofont-home"></i>
              </a>
            </li>
            <li class="breadcrumb-item"><a href="">Daftar Calon Siswa</a></li>
          </ol>
        </div>
      </div>
    </div>
     <!-- Container-fluid ends -->
    @if(Session::has('nextdownload'))
        <a target="_blank" class="btn btn-info waves-effect waves-light m-r-30" href="{{  url("simpan/".Session::get('nextdownload')) }}"> Download PDF LOGIN File</a>
    @endif
    <!--<a href="{{url('admin/siswa/create')}}">Tambah</a>-->
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header"><h5 class="card-header-text">Daftar Login</h5></div>
          <div class="card-block">
            <table id="advanced-table" class="table dt-responsive table-striped table-bordered nowrap">
              <thead>
              <tr>
                <th>Email</th>
                <th>Details</th>
                
              </tr>
              </thead>
              <tfoot>
              <tr>
                <th>Email</th>
                <th>Details</th>
                
              </tr>
              </tfoot>
              <tbody>
               @foreach($siswa as $data)
              <tr>
                <td>{{$data->email}}</td>
                <td><a href="{{url('/admin/siswa/detail/'.$data->id)}}">Detail</a></td>
               
              </tr>
               @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
