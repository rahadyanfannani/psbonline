@extends('layouts.admin')
@section('content')
 <!-- Container-fluid starts -->
    <!-- Main content starts -->
    
            <!-- Main content starts -->
            <div >

                 <div class="row">
                    <div class="col-sm-12 p-0">
                        <div class="main-header">
                           
                            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="{{ url('admin/siswa') }}"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="">Dashboard</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- Row end -->
                <div class="row">
                    <!-- Form Control starts -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">Data Calon Siswa</h5>
                            </div> 
                            <div class="card-block">
<form action="{{ url('admin/simpan')  }}" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Nomor Ujian</label>
            <input type="text" name="nomorujian" class="form-control" value="{{ $siswa->nomor_ujian  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">NISN</label>
            <input type="text" name="nisn" class="form-control"  value="{{ $siswa->nisn  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Nama Lengkap</label>
            <input type="text" name="nama" class="form-control"  value="{{ $siswa->nama  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText2" class="form-control-label">Jenis Kelamin</label>
                <div class="form-check">
                    <label for="optionsRadios2" class="form-check-label">
                    <input type="radio" class="form-check-input" name="jeniskelamin" value="l" {{ ($siswa->jenis_kelamin == 'l') ? "checked" : "" }}  {{($siswa->status == 1) ? "disabled" : ""}}>Laki Laki</label>
                </div>
                <div class="form-check">
                    <label for="optionsRadios2" class="form-check-label">
                    <input type="radio" class="form-check-input" name="jeniskelamin" value="p" {{($siswa->status == 1) ? "disabled" : ""}} {{ ($siswa->jenis_kelamin == 'p') ? "checked" : "" }} >Perempuan</label>
                </div>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Tempat Lahir</label>
            <input type="text" name="tempat" class="form-control" value="{{ $siswa->tempat  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Tanggal Lahir</label>
            <input type="date" name="tanggallahir" class="form-control"  value="{{ $siswa->tanggal_lahir  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Alamat</label>
            <input type="text" name="alamat" class="form-control"  value="{{ $siswa->alamat  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">RT</label>
            <input type="text" name="rt" class="form-control" value="{{ $siswa->rt     }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">RW</label>
            <input type="text" name="rw" class="form-control" value="{{ $siswa->rw  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Kelurahan</label>
            <input type="text" name="kelurahan" class="form-control" value="{{ $siswa->kelurahan  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Kecamatan</label>
            <input type="text" name="kecamatan" class="form-control" value="{{ $siswa->kecamatan  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Kota</label>
            <input type="text" name="kota" class="form-control" value="{{ $siswa->kota  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Asal Sekolah</label>
            <input type="text" name="asalsekolah" class="form-control"  value="{{ $siswa->asal_sekolah  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Telepon</label>
            <input type="text" name="telepon" class="form-control" value="{{ $siswa->no_hp  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Total Nilai UN</label>
            <input type="text" name="totalnilaiun" class="form-control" value="{{ $siswa->total_nilai_un  }}" {{($siswa->status == 1) ? "disabled" : ""}}>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
<input type="submit" class="btn btn-success waves-effect waves-light m-r-30" value="Simpan">
     </div>

                </div>
            </div>

    <div class="row">
                    <!-- Form Control starts -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">Upload Dokumen</h5>
                            </div> 
<div class='alert alert-info alert-dismissible fade in' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button> <p>Tidak perlu meng-upload data,dinonaktifkan oleh administrator</p></div>
                            <div class="card-block">
    @if($siswa->status == 0)
    <div class="form-group row">
        <label for="example-text-input" class="col-xs-2 col-form-label form-control-label">Akte</label>
        <div class="col-sm-10">
        <input type="file" name="akte" class="form-control" disabled="disabled">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-text-input" class="col-xs-2 col-form-label form-control-label">KTP</label>
        <div class="col-sm-10">
        <input type="file" name="ktp" class="form-control" disabled="disabled">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-text-input" class="col-xs-2 col-form-label form-control-label">KK</label>
        <div class="col-sm-10">
        <input type="file" name="kk" class="form-control" disabled="disabled">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-text-input" class="col-xs-2 col-form-label form-control-label">Ijazah</label>
        <div class="col-sm-10">
        <input type="file" name="ijazah" class="form-control" disabled="disabled">
        </div>
    </div>
    @endif




        @if ($siswa->akte)
        <a target="_blank" class="btn btn-info waves-effect waves-light m-r-30" href="{{url("storage/images/".$siswa->token."_akte.".$siswa->kk)}}">Download Akte</a>
        @endif


        @if ($siswa->kk)
            <a target="_blank" class="btn btn-info waves-effect waves-light m-r-30" href="{{url("storage/images/".$siswa->token."_kk.".$siswa->kk)}}">Download KK</a>
        @endif
        @if ($siswa->ktp)
            <a target="_blank" class="btn btn-info waves-effect waves-light m-r-30" href="{{url("storage/images/".$siswa->token."_ktp.".$siswa->kk)}}">Download KTP</a>
        @endif


        @if ($siswa->ijazah)
            <a target="_blank" class="btn btn-info waves-effect waves-light m-r-30" href="{{url("storage/images/".$siswa->token."_ijazah.".$siswa->kk)}}">Download Ijazah</a>
        @endif



    @if($siswa->status == 0)
    <input type="submit" class="btn btn-success waves-effect waves-light m-r-30" value="Simpan">
    @endif
    @if($siswa->status == 1)
        <a class="btn btn-warning waves-effect waves-light m-r-30" href="{{url('admin/siswa/validate/'.$siswa->id)}}">Buka Validasi</a>
    @else
        <a class="btn btn-danger waves-effect waves-light m-r-30" href="{{url('admin/siswa/validate/'.$siswa->id)}}">Validasi</a>
    @endif

</form>
                    </div>

                </div>
            </div>
    </div>

@endsection