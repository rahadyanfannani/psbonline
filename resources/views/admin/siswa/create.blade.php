@extends('layouts.admin')
@section('content')
 <!-- Container-fluid starts -->
    <!-- Main content starts -->
    
            <!-- Main content starts -->
            <div >

                 <div class="row">
                    <div class="col-sm-12 p-0">
                        <div class="main-header">
                           
                            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="{{ url('admin/siswa') }}"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="">Tambah Calon Siswa</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- Row end -->
                <div class="row">
                    <!-- Form Control starts -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">Tambah Calon Siswa</h5>
                            </div> 
                            <div class="card-block">
<form action="{{url('admin/siswa')}}" method="post">
    {{csrf_field()}}
    <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Masukkan Alamat Email</label>
            <input type="email" name="email"  class="form-control" placeholder="Masukkan Email">
            <small id="emailHelp" class="form-text text-muted">Email aktif agar server mengirim login ke email</small>
        </div>
    <div class="form-group">
        <label for="exampleInputText1" class="form-control-label">Masukkan Nama</label>
        <input type="text" name="nama"  class="form-control" placeholder="Masukkan Nama">
    </div>
        <div class="form-group">
            <label for="exampleInputText1" class="form-control-label">Nomor HP</label>
            <input type="text" name="no_hp" class="form-control" placeholder="Masukkan no Hp">
            <small id="emailHelp" class="form-text text-muted">NO. HP aktif agar sewaktu bisa dihubungin</small>
        </div>

    <input type="submit" class="btn btn-success waves-effect waves-light m-r-30"  value="Tambah Calon Siswa">
</form>
                    </div>

                </div>
            </div>
    </div>

@endsection