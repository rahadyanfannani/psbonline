<!DOCTYPE html>
<html>
<head>
    <title>LOGIN</title>
    <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="-">
    <meta name="keywords" content="-">
    <meta name="author" content="-">
</head>
<body>
 <div class="loader-bg">
    <div class="loader-bar">
    </div>
  </div>
<div style="background:#f2f2f2;margin:0 auto;max-width:640px;padding:0 20px">
    <table align="center" border="0" cellpadding="0" cellspacing="0">
        <tbody>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <div style="width:96%;margin:auto;padding:5px 0 0px 0;text-align:center">
                    
                </div>
                <div style="text-align: center;background-color: #2196F3;color: #Fff;font-family:'Open Sans', sans-serif;font-size:13px; padding: 1px;margin-top: 10px;border-radius:10px 10px 0 0;">
                    <h2>Sign up</h2>
                </div>
                <div style="background:#fff;color:#5b5b5b;font-family:'Open Sans', sans-serif;font-size:13px;padding:10px 20px;margin:20px auto;line-height:17px;border:1px #ddd solid;border-top:0;clear:both;margin-top: 0;border-radius: 0 0 10px 10px;">
                    <p>Greetings from PPDB MBS KOTA BEKASI 2017</p>
                    <p>Dear {{ $email  }},</p>
                    <p>Thank you for signup.</p>
                    <p><b>Your Username</b></p>
                    <div style="text-align:center;margin:15px;font-size: 32px;font-weight: bold;text-decoration: underline;padding: 0 2px;">
                        {{ $username  }}
                    <p><b>Your Password</b></p>
                    <div style="text-align:center;margin:15px;font-size: 32px;font-weight: bold;text-decoration: underline;padding: 0 2px;">
                     
                        {{ $password  }}
                    </div><br><br><br>
                    <div style="text-align:center;margin:15px"> <a href="https://ppdb.mbs.kota-bekasi.id/login" style="display:inline-block; padding: 10px;background-color: #1b8bf9;font-size: 15px;color: #fff;border-radius: 5px;text-decoration:none;">Login Now</a> </div>

                </div>
                </td>
        </tr>
        </tbody>
    </table>
</div>
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 9]>
      <div class="ie-warning">
          <h1>Warning!!</h1>
          <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
          <div class="iew-container">
              <ul class="iew-download">
                  <li>
                      <a href="http://www.google.com/chrome/">
                          <img src="assets/images/browser/chrome.png" alt="Chrome">
                          <div>Chrome</div>
                      </a>
                  </li>
                  <li>
                      <a href="https://www.mozilla.org/en-US/firefox/new/">
                          <img src="assets/images/browser/firefox.png" alt="Firefox">
                          <div>Firefox</div>
                      </a>
                  </li>
                  <li>
                      <a href="http://www.opera.com">
                          <img src="assets/images/browser/opera.png" alt="Opera">
                          <div>Opera</div>
                      </a>
                  </li>
                  <li>
                      <a href="https://www.apple.com/safari/">
                          <img src="assets/images/browser/safari.png" alt="Safari">
                          <div>Safari</div>
                      </a>
                  </li>
                  <li>
                      <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                          <img src="assets/images/browser/ie.png" alt="">
                          <div>IE (9 & above)</div>
                      </a>
                  </li>
              </ul>
          </div>
          <p>Sorry for the inconvenience!</p>
      </div>
      <![endif]-->
<!-- Warning Section Ends -->
</body>
</html>
