<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Sekolah;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Admin;
use App\Siswa;


class SuperAdminController extends Controller
{
    public function showAccount(){
        $data['admin'] = Account::where('level','1')->get();
        $data['siswa'] = Siswa::all();
        $data['sekolah'] = Sekolah::all();
        return view('superadmin.index',$data);
    }
    public function createAdmin(){
        $data['sekolah'] = Sekolah::all();
        return view('superadmin.account.create',$data);
    }
    public function storeAdmin(Request $request){
        $data = new Account;
        $data->level = 1;
        $data->email = $request->email;

        $admin = new Admin;
        $admin->email = $request->email;
        $admin->nama = $request->nama;
        $admin->sekolah = $request->sekolah;

        $admin->save();
        $pass = $this->generateAlphanumeric();
        $data->password = bcrypt($pass);
        $data->save();


        $mail['email'] = $admin->email;
        $mail['password'] = $pass;
        $mail['generate'] = $pass;


        Mail::send('emails.siswa', $mail, function ($message) use ($request) {
            $message->from('no-reply@kota-bekasi.id', 'Registrasi Admin');
            $message->to($request->email)->subject('Registrasi Admin');
        });

        return redirect('superadmin/');
    }

    public function createSekolah(){

        return view('superadmin.sekolah.create');
    }
    public function storeSekolah(Request $request){


        $sekolah = new Sekolah;

        $sekolah->nama = $request->nama;
        $sekolah->alamat = $request->alamat;
        $sekolah->rt = $request->rt;
        $sekolah->rw = $request->rw;
        $sekolah->kelurahan = $request->kelurahan;
        $sekolah->kecamatan = $request->kecamatan;
        $sekolah->kota = $request->kota;
        $sekolah->username = $request->username;
        $pass = $this->generateAlphanumeric();
        $sekolah->password = bcrypt($pass);
        $sekolah->email = $request->email;
        $sekolah->save();

        $mail['email'] = $sekolah->email;
        $mail['password'] = $pass;
        $mail['username'] = $sekolah->username;


        Mail::send('emails.siswa', $mail, function ($message) use ($request) {
            $message->from('no-reply@kota-bekasi.id', 'Registrasi Admin');
            $message->to($request->email)->subject('Registrasi Admin');
        });


        return redirect('superadmin');
    }

    private function generateAlphanumeric(){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < 7; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }

    public function showLog(){
        $data['data'] = Log::all();
        return view('superadmin.log');
    }
    public function getLogin(){
        $data['level'] = "superadmin";
        return view('admin.login',$data);
    }
    public function postLogin(Request $request){

        $data = Account::where("email",$request->email)->where('level',3)->first();

        if($data){

            if(Hash::check($request->password,$data->password)  && $data->level == 3){
                session(['email' =>  $request->email,'level'=>$data->level]);
                return redirect('superadmin/');
            } else {
                return redirect('superadmin/login')->with('error', 'email dan password anda salah');
            }
        } else {
            return redirect('superadmin/login')->with('error', 'email dan password anda salah');
        }
    }
    public function logout(Request $request){
        $request->session()->flush();
        return redirect('login');
    }
    public function gantiPassword(){
        return view('superadmin.ganti');
    }
    public function postGantiPassword(Request $request){
        $account = Account::where('email',session('email'))->first();
        $account->password = bcrypt($request->password);
        $account->save();
        return redirect('logout');


    }
    public function resetSekolah(Request $request,$id){
        $pass = $this->generateAlphanumeric();

        $sekolah = Sekolah::find($id);
        $sekolah->password = bcrypt($pass);
        $sekolah->save();

        $data['email'] = $sekolah->email;
        $data['password'] = $pass;
        $data['username'] = $sekolah->username;

        Mail::send('emails.siswa', $data, function ($message) use ($request) {
            $message->from('no-reply@kota-bekasi.id', 'Reset Password');
            $message->to($request->email)->subject("Reset Password");
        });

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('emails.siswa', $data);
        $random = md5(md5($sekolah->id));

        File::makeDirectory("./simpan/".$random, 0775);

        $pdf->save("./simpan/".$random."/".$sekolah->id."sekolah.pdf");
        Session::flash('nextdownload', $random."/".$sekolah->id."sekolah.pdf");



    }
    public function resetSiswa(Request $request,$id){
        $pass = $this->generateAlphanumeric();

        $siswa = Siswa::find($id);
        $siswa->password = bcrypt($pass);
        $siswa->save();

        $data['email'] = $siswa->email;
        $data['password'] = $pass;
        $data['username'] = $siswa->username;

        Mail::send('emails.siswa', $data, function ($message) use ($request) {
            $message->from('no-reply@kota-bekasi.id', 'Reset Password');
            $message->to($request->email)->subject("Reset Password");
        });

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('emails.siswa', $data);
        $random = md5(md5($siswa->id));

        File::makeDirectory("./simpan/".$random, 0775);
        $pdf->save("./simpan/".$random."/".$siswa->id."siswa.pdf");
        Session::flash('nextdownload', $random."/".$siswa->id."siswa.pdf");



    }




}
