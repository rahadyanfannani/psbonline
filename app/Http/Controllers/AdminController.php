<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Siswa;
use App\Account;
use Illuminate\Support\Facades\Mail;
use App\Mail\SiswaEmail;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Admin;
use App\Sekolah;

class AdminController extends Controller
{
    public function getLogin(){
        $data['level'] = "admin";
        return view('admin.login',$data);
    }
    public function postLogin(Request $request){
        $data = Account::where("email",$request->email)->where('level',1)->first();
        $admin = Admin::where('email',$request->email)->first();
        if($data){
            if(Hash::check($request->password,$data->password)  && $data->level == 1){
                session(['sekolah' => $admin->sekolah, 'email' => $request->email,'level'=>$data->level]);
                return redirect('admin/siswa');
            } else {
                return redirect('admin/login')->with('error', 'email dan password anda salah');
            }
        } else {
            return redirect('admin/login')->with('error', 'email dan password anda salah');
        }
    }
    public function showDashboard(){

    }

    public function showSiswa(){
        $sekolah = Sekolah::where('username',session('username'))->first();
        $data['siswa'] = Siswa::where('asal_sekolah',$sekolah->id)->get();
        return view('admin.siswa.show',$data);
    }
    public function showSiswaForm(){
        return view('admin.siswa.create');
    }
    public function createSiswa(Request $request){


        $pass = $this->generateAlphanumeric();
        $enkripsi =  bcrypt($pass);
        $siswa = new Siswa;
        $siswa->password =$enkripsi;
        $siswa->email = $request->email;
        $siswa->no_hp = $request->no_hp;
        $sekolah = Sekolah::where('username',session('username'))->first();
        $siswa->asal_sekolah = $sekolah->id;
        $siswa->status = 0;
        $siswa->nama = $request->nama;

        $siswa->username = substr(str_replace(' ', '', strtoupper($request->nama)),0,5).$this->generateRandom();
        $siswa->save();
        $data['email'] = $siswa->email;
        $data['password'] = $pass;
        $data['username'] = $siswa->username;


        Mail::send('emails.siswa', $data, function ($message) use ($request) {
            $message->from('no-reply@kota-bekasi.id', 'Pendaftaran Online');
            $message->to($request->email)->subject("Pendaftaran Online");
        });
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('emails.siswa', $data);
        $random = md5(md5($siswa->id));

        $siswa->token = $random;
        $siswa->save();
        File::makeDirectory("./simpan/".$random, 0775);
        $pdf->save("./simpan/".$random."/".$siswa->id.".pdf");
        Session::flash('nextdownload', $random."/".$siswa->id.".pdf");
        return redirect('/admin/siswa');

    }
    private function generateRandom(){
        $characters = '0123456789';
        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < 5; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }
    private function generateAlphanumeric(){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < 7; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }
    public function detailSiswa($id){
        $data['siswa'] = Siswa::find($id);
        return view('admin.siswa.detail',$data);
    }
    public function simpanSiswa(Request $request){
        $siswa = Siswa::find(session('idlogin'));
        $siswa->nama = $request->nama;
        $siswa->nomor_ujian = $request->nomorujian;
        $siswa->no_hp = $request->telepon;
        $siswa->nisn = $request->nisn;
        $siswa->jenis_kelamin = $request->jeniskelamin;
        $siswa->tempat = $request->tempat;
        $siswa->tanggal_lahir = $request->tanggallahir;
        $siswa->alamat = $request->alamat;
        $siswa->rt = $request->rt;
        $siswa->rw = $request->rw;
        $siswa->kelurahan = $request->kelurahan;
        $siswa->kecamatan = $request->kecamatan;
        $siswa->kota = $request->kota;

        $siswa->total_nilai_un = $request->totalnilaiun;
        $siswa->save();

        $allow = array('jpg','jpeg','png','pdf');



        $extension[0] = ($request->hasFile('akte')) ? Input::file('akte')->getClientOriginalExtension() : null;
        $extension[1] = ($request->hasFile('kk')) ? Input::file('kk')->getClientOriginalExtension() : null;
        $extension[2] = ($request->hasFile('ktp')) ? Input::file('ktp')->getClientOriginalExtension() : null;
        $extension[3] = ($request->hasFile('ijazah')) ? Input::file('ijazah')->getClientOriginalExtension() : null;
        $valid[0] = false;
        $valid[1] = false;
        $valid[2] = false;
        $valid[3] = false;
        foreach($extension as $index=> $data ){
            foreach($allow as $ex){
                if($data == $ex){
                    $valid[$index] = true;
                    break;
                }
            }
        }




        if($extension[0] != null && $valid[0]) {
            $siswa->akte = $extension[0];
            $path = $request->akte->storeAs('images', $siswa->token.'_akte.jpg',"public");
        }
        if($extension[1] != null && $valid[1]) {
            $siswa->kk = $extension[1];
            $path = $request->kk->storeAs('images', $siswa->token.'_kk.jpg',"public");
        }
        if($extension[2] != null && $valid[2]) {
            $siswa->ktp = $extension[2];
            $path = $request->ktp->storeAs('images', $siswa->token.'_ktp.jpg',"public");
        }
        if($extension[3] != null && $valid[3]) {
            $siswa->ijazah = $extension[3];
            $path = $request->ijazah->storeAs('images', $siswa->token.'_ijazah.jpg',"public");
        }
        $siswa->save();


        if(($extension[0] != null && !$valid[0])){
            Session::flash('error1', "Extension file akte tidak valid");
        }
        if(($extension[1] != null && !$valid[1])){
            Session::flash('error2', "Extension file kk tidak valid");

        }
        if(($extension[2] != null && !$valid[2])){
            Session::flash('error3', "Extension file ktp tidak valid");

        }
        if(($extension[3] != null && !$valid[3])){
            Session::flash('error4', "Extension file ijazah tidak valid");
        }
        return redirect('admin/siswa/detail/'.$siswa->id);

    }
    public function validateSiswa($id){
        $siswa = Siswa::find($id);
        $siswa->status = ($siswa->status == 1) ? 0 : 1;
        $siswa->save();
        return redirect('admin/siswa/detail/'.$id);
     }

     public function logout(Request $request){
        $request->session()->flush();
        return redirect('login');
     }








}
