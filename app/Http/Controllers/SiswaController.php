<?php

namespace App\Http\Controllers;

use App\Account;
use App\ViewLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Siswa;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Sekolah;

class SiswaController extends Controller
{
    public function getLogin(){
        return view('siswa.login');
    }
    public function postLogin(Request $request){
        //return bcrypt('admin');
        $data = ViewLogin::where("username",$request->username)->first();

        if($data){

            if(Hash::check($request->password,$data->password) ){
                session(['username' =>  $request->username,'level'=>$data->level,'nama'=>$data->nama]);
                if($data->level == 1){
                    return redirect('superadmin');
                } else if($data->level == 2){
                    return redirect('admin');
                } else if($data->level == 3){
                    return redirect('siswa');
                }

            } else {
                return redirect('login')->with('error', 'username dan password anda salah');
            }
        } else {
            return redirect('login')->with('error', 'username dan password anda salah');
        }
    }
    public function dashboard(Request $request){
        $data['siswa'] = Siswa::where('username',session('username'))->first();
        $data['sekolah'] = Sekolah::find($data['siswa']->asal_sekolah);

        return view('siswa.dashboard',$data);
    }
    public function saveSiswa(Request $request){
        $siswa = Siswa::where('username',session('username'))->first();
        $siswa->nama = $request->nama;
        $siswa->nomor_ujian = $request->nomorujian;
        $siswa->no_hp = $request->telepon;
        $siswa->nisn = $request->nisn;
        $siswa->jenis_kelamin = $request->jeniskelamin;
        $siswa->email = $request->email;
        $siswa->tempat = $request->tempat;

        $siswa->tanggal_lahir = $request->tanggallahir;
        $siswa->alamat = $request->alamat;
        $siswa->rt = $request->rt;
        $siswa->rw = $request->rw;
        $siswa->kelurahan = $request->kelurahan;
        $siswa->kecamatan = $request->kecamatan;
        $siswa->kota = $request->kota;

        $siswa->total_nilai_un = $request->totalnilaiun;
        $siswa->save();
        //$siswa->username = $

        $allow = array('jpg','jpeg','png','pdf');



        $extension[0] = ($request->hasFile('akte')) ? Input::file('akte')->getClientOriginalExtension() : null;
        $extension[1] = ($request->hasFile('kk')) ? Input::file('kk')->getClientOriginalExtension() : null;
        $extension[2] = ($request->hasFile('ktp')) ? Input::file('ktp')->getClientOriginalExtension() : null;
        $extension[3] = ($request->hasFile('ijazah')) ? Input::file('ijazah')->getClientOriginalExtension() : null;
        $valid[0] = false;
        $valid[1] = false;
        $valid[2] = false;
        $valid[3] = false;
        foreach($extension as $index=> $data ){
            foreach($allow as $ex){
                if($data == $ex){
                    $valid[$index] = true;
                    break;
                }
            }
        }


        if($extension[0] != null && $valid[0]) {
            $siswa->akte = $extension[0];
            $path = $request->akte->storeAs('images', $siswa->token.'_akte.jpg',"public");
        }
        if($extension[1] != null && $valid[1]) {
            $siswa->kk = $extension[1];
            $path = $request->kk->storeAs('images', $siswa->token.'_kk.jpg',"public");
        }
        if($extension[2] != null && $valid[2]) {
            $siswa->ktp = $extension[2];
            $path = $request->ktp->storeAs('images', $siswa->token.'_ktp.jpg',"public");
        }
        if($extension[3] != null && $valid[3]) {
            $siswa->ijazah = $extension[0];
            $path = $request->ijazah->storeAs('images', $siswa->token.'_ijazah.jpg',"public");
        }
        $siswa->save();


        if(($extension[0] != null && !$valid[0])){
            Session::flash('error1', "Extension file akte tidak valid");
        }
        if(($extension[1] != null && !$valid[1])){
            Session::flash('error2', "Extension file kk tidak valid");

        }
        if(($extension[2] != null && !$valid[2])){
            Session::flash('error3', "Extension file ktp tidak valid");

        }
        if(($extension[3] != null && !$valid[3])){
            Session::flash('error4', "Extension file ijazah tidak valid");
        }


        return redirect('siswa');






    }
    public function logout(Request $request){
        $request->session()->flush();
        return redirect('login');
    }






}
