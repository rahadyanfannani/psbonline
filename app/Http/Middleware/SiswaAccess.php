<?php

namespace App\Http\Middleware;

use Closure;

class SiswaAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('username') || session('level') != 3) {
            return redirect('/login');
        }
        return $next($request);
    }
}
