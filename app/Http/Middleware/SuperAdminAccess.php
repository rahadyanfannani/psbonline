<?php

namespace App\Http\Middleware;

use Closure;

class SuperAdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('username') || session('level') != 1) {
            return redirect('superadmin/login');
        }
        return $next($request);
    }
}
