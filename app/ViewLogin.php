<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewLogin extends Model
{
    protected $table = "view_login";
    protected $primaryKey = "username";
    public $timestamps = false;

}
