<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = "siswa";
    public $timestamps = false;
    public function sekolah(){
        return $this->belongsTo('App\Sekolah', 'asal_sekolah', 'id');
    }
}
