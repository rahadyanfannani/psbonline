<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['middleware' => 'admin'], function () {
    Route::get('admin', 'AdminController@showSiswa');
    Route::get('admin/siswa/', 'AdminController@showSiswa');
    Route::get('admin/siswa/create', "AdminController@showSiswaForm");
    Route::post('admin/siswa', "AdminController@createSiswa");
    Route::get('admin/siswa/detail/{id}', "AdminController@detailSiswa");
    Route::post('admin/simpan', "AdminController@simpanSiswa");
    Route::get('admin/siswa/validate/{id}', "AdminController@validateSiswa");
    Route::get('admin/logout','AdminController@logout');
});

Route::get('login', 'SiswaController@getLogin');
Route::post('login', 'SiswaController@postLogin');

Route::group(['middleware' => 'siswa'], function () {
    Route::get('siswa', "SiswaController@dashboard");
    Route::post('siswa/simpan', 'SiswaController@saveSiswa');
    Route::get('siswa/logout','SiswaController@logout');

});


Route::group(['middleware' => 'superadmin'], function () {
    Route::get('superadmin/account', "SuperAdminController@createAdmin");
    Route::post('superadmin/account', "SuperAdminController@storeAdmin");
    Route::get('superadmin/sekolah', "SuperAdminController@createSekolah");
    Route::post('superadmin/sekolah', "SuperAdminController@storeSekolah");
    Route::get('superadmin', "SuperAdminController@showAccount");
    Route::get('superadmin/log', "SuperAdminController@showLog");
    Route::get('superadmin/logout', "SuperAdminController@logout");
    Route::get('superadmin/gantipassword', "SuperAdminController@gantiPassword");
    Route::post('superadmin/gantipassword', "SuperAdminController@postGantiPassword");
    Route::get('siswa/reset/{id}','SuperAdminController@resetSiswa');
    Route::get('sekolah/reset/{id}','SuperAdminController@resetSekolah');
});


Route::get('/', function () {
    return view('welcome');
});